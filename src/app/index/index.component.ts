import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-index',
  templateUrl: './index.component.html',
  styleUrls: ['./index.component.css']
})
export class IndexComponent implements OnInit {

  restData: string

  constructor() { }

  ngOnInit(): void {

    this.getData()

  }

  getData(){
    fetch(`https://api.cii3.net/rest/test.php`).then(e=>{
      return e.json()
    }).then(e=>{
      console.log(e)
      this.restData = e
    })
  }

}
