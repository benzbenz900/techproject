<?php
header("Access-Control-Allow-Origin: *"); //ไม่แนะนำให้ ใช้ * แทนโดนเมน
header('Content-Type: application/json');
?>
[
    {
        "id": "7439",
        "image": "https://spacecdn.sheepola.com/static/imgs/products/medium/hKc14ZZicT6iTBIbydFUFM8nFTMOt8.jpg",
        "name": "กระเป๋าเดินทางโครงอลูมิเนียม แพ็กคู่ (สีชมพู)",
        "s_price": "3,990.00",
        "n_price": "2,990.00",
        "discount": "ลด 25%"
    },
    {
        "id": "7307",
        "image": "https://spacecdn.sheepola.com/static/imgs/products/medium/UgmBWNppvDA6f5EDlcitQ9Sb1m3RJi.jpg",
        "name": "ที่นอนสัตว์เลี้ยง ที่นอนหมาแมว ทรงสี่เหลี่ยม Size.XL (สีส้ม)",
        "s_price": "699.00",
        "n_price": "499.00",
        "discount": "ลด 29%"
    },
    {
        "id": "7260",
        "image": "https://spacecdn.sheepola.com/static/imgs/products/medium/h1JqkZe8nhLDRRncJdSU6Q2NPWPTni.jpg",
        "name": "ที่นอนโดม อุโมงค์สัตว์เลี้ยง ลายน้องเหมียว สีเหลือง",
        "s_price": "650.00",
        "n_price": "499.00",
        "discount": "ลด 23%"
    }
]